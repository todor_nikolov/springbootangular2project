import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `
  	<nav>
  		<a routerLink="/new" routerLinkActive="active">Create Organization</a> |
  		<a routerLink="/all" routerLinkActive="active">View All</a>
  	</nav>
  	<router-outlet></router-outlet>
  	`,
  	styleUrls: ['app/app.component.css']
})
export class AppComponent { }