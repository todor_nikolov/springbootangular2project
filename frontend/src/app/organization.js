"use strict";
var Organization = (function () {
    function Organization(id, name, regNumber, regDate, owner, address, postalCode, activityType, description) {
        this.id = id;
        this.name = name;
        this.regNumber = regNumber;
        this.regDate = regDate;
        this.owner = owner;
        this.address = address;
        this.postalCode = postalCode;
        this.activityType = activityType;
        this.description = description;
    }
    return Organization;
}());
exports.Organization = Organization;
//# sourceMappingURL=organization.js.map