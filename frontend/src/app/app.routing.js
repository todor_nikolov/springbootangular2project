"use strict";
var router_1 = require('@angular/router');
var organizations_component_1 = require('./organizations.component');
var organization_form_component_1 = require('./organization-form.component');
var appRoutes = [
    {
        path: 'all',
        component: organizations_component_1.OrganizationsComponent
    },
    {
        path: 'new',
        component: organization_form_component_1.OrganizationFormComponent
    },
    {
        path: '',
        redirectTo: '/new',
        pathMatch: 'full'
    }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map