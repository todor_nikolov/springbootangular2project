export class Organization{

	constructor (
		public id: number,
		public name: string,
		public regNumber : string,
		public regDate : Date,
		public owner : string,
		public address : string,
		public postalCode : string,
		public activityType : string,
		public description : string
	){}
	
}