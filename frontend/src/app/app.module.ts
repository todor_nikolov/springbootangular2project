import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http'; 

import { AppComponent }   from './app.component';
import { OrganizationService} from './organization.service';
import { OrganizationsComponent} from './organizations.component';
import { OrganizationFormComponent} from './organization-form.component';
import { routing } from './app.routing';
import 'app/rxjs-extensions';

@NgModule({
  imports: [ 
  	BrowserModule,
  	FormsModule,
  	HttpModule,
  	routing 
  ],
  
  declarations: [ 
  	AppComponent, 
  	OrganizationsComponent,
  	OrganizationFormComponent 
  ],
  providers: [ OrganizationService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }