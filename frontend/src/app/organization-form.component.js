"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var organization_1 = require('./organization');
var organization_service_1 = require('./organization.service');
var Rx_1 = require("rxjs/Rx");
var OrganizationFormComponent = (function () {
    function OrganizationFormComponent(organizationServise) {
        this.organizationServise = organizationServise;
        this.active = true;
        this.submitted = false;
        this.model = new organization_1.Organization(12, '', '', null, '', '', '', '', '');
    }
    OrganizationFormComponent.prototype.onSubmit = function () {
        this.submitted = true;
        this.organizationServise.createOrganization(JSON.stringify(this.model)).subscribe(function (data) {
            return true;
        }, function (error) {
            return Rx_1.Observable.throw(error);
        });
    };
    OrganizationFormComponent.prototype.onSubmitted = function () {
        this.submitted = false;
        this.onReset();
    };
    OrganizationFormComponent.prototype.onReset = function () {
        var _this = this;
        this.model = new organization_1.Organization(12, '', '', null, '', '', '', '', '');
        this.active = false;
        setTimeout(function () { return _this.active = true; }, 0);
    };
    OrganizationFormComponent = __decorate([
        core_1.Component({
            selector: 'organization-form',
            templateUrl: 'app/organization-form.component.html'
        }), 
        __metadata('design:paramtypes', [organization_service_1.OrganizationService])
    ], OrganizationFormComponent);
    return OrganizationFormComponent;
}());
exports.OrganizationFormComponent = OrganizationFormComponent;
//# sourceMappingURL=organization-form.component.js.map