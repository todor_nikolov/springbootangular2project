import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrganizationsComponent} from './organizations.component';
import { OrganizationFormComponent} from './organization-form.component';

const appRoutes : Routes = [
	{
		path: 'all',
		component: OrganizationsComponent
	},
	{
		path: 'new',
		component: OrganizationFormComponent
	},
	{
		path: '',
		redirectTo: '/new',
		pathMatch:'full'
	}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);