import { Component } from '@angular/core';

import { Organization } from './organization';
import { OrganizationService } from './organization.service';

import {Observable} from "rxjs/Rx";

@Component({
	selector: 'organization-form',
	templateUrl: 'app/organization-form.component.html'
})
export class OrganizationFormComponent{
	active = true;
	submitted = false;
	
	model = new Organization(12, '', '', null, '', '', '', '', '');
	
	constructor(private organizationServise: OrganizationService) { }
  	
  	onSubmit() { 

  		this.submitted = true;
		this.organizationServise.createOrganization(JSON.stringify(this.model)).subscribe(
	    	data => {
	    		return true;
	       	},
	       	error => {
	         return Observable.throw(error);
	       	}
    	);
  	}

  	onSubmitted() {
  		this.submitted = false;
  		this.onReset();
  	}

  	onReset() {
  		this.model = new Organization(12, '', '', null, '', '', '', '', '');
    	this.active = false;
    	setTimeout(() => this.active = true, 0);
  	}

}