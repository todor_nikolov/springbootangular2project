import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Request, RequestMethod, Response } from '@angular/http';

import {Observable} from 'rxjs/Rx';

import { Organization } from './organization';

@Injectable()
export class OrganizationService{

	private serverUrl = 'http://localhost:8080';
	private createOrganizationServerController = '/new'
	private organizationsServerController = '/all';
	
	constructor(private http: Http) { }

	createOrganization(body) : Observable<Response>{
	    let headers = new Headers({ 'Accept': 'application/json', 'Content-Type': 'application/json' });
	    let options = new RequestOptions({ headers: headers });
	    return this.http.post(this.serverUrl + this.createOrganizationServerController, body, headers)
	    			.map((response: Response) => response.json());
  	}

	getOrganiztions(): Observable<Organization[]> {
    	return this.http.get(this.serverUrl + this.organizationsServerController)
    				.map(this.extractData)
                    .map((organizations: Array<any>) => {
                        
                        let result:Array<Organization> = [];

                        if (organizations) {
                        	organizations.forEach((organization) => {
                            	result.push(new Organization(organization.id, organization.name, 
                                organization.regNumber, organization.regDate, organization.owner, 
                                organization.address, organization.postalCode, 
                                organization.activityType, organization.description));
                        		
                        	});
                    	}
                        
						return result;
                    
                    });
  	
	}

	private extractData(response: Response) {
    	let body = response.json();
    	return body;
	}

	private handleError(error: any): Promise<any> {
  		console.error('An error occurred', error); // for demo purposes only
  		return Promise.reject(error.message || error);
	}
}