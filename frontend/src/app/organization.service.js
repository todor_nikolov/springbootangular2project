"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var organization_1 = require('./organization');
var OrganizationService = (function () {
    function OrganizationService(http) {
        this.http = http;
        this.serverUrl = 'http://localhost:8080';
        this.createOrganizationServerController = '/new';
        this.organizationsServerController = '/all';
    }
    OrganizationService.prototype.createOrganization = function (body) {
        var headers = new http_1.Headers({ 'Accept': 'application/json', 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post(this.serverUrl + this.createOrganizationServerController, body, headers)
            .map(function (response) { return response.json(); });
    };
    OrganizationService.prototype.getOrganiztions = function () {
        return this.http.get(this.serverUrl + this.organizationsServerController)
            .map(this.extractData)
            .map(function (organizations) {
            var result = [];
            if (organizations) {
                organizations.forEach(function (organization) {
                    result.push(new organization_1.Organization(organization.id, organization.name, organization.regNumber, organization.regDate, organization.owner, organization.address, organization.postalCode, organization.activityType, organization.description));
                });
            }
            return result;
        });
    };
    OrganizationService.prototype.extractData = function (response) {
        var body = response.json();
        return body;
    };
    OrganizationService.prototype.handleError = function (error) {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    OrganizationService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], OrganizationService);
    return OrganizationService;
}());
exports.OrganizationService = OrganizationService;
//# sourceMappingURL=organization.service.js.map