import { Component, OnInit } from '@angular/core';

import {Organization} from './organization';
import { OrganizationService } from './organization.service';

@Component({
  	selector: 'organization-list',
  	templateUrl: 'app/organizations.component.html',
	styleUrls: ['tables.css', 'app/organizations.component.css'],
	providers : [ OrganizationService ]
})
export class OrganizationsComponent implements OnInit{ 
	title = 'Organization form';
	
	organizations : Organization[];
 	
 	public foods_error:Boolean = false;

	constructor(private organizationServise: OrganizationService){}
	
	ngOnInit() : void {
		this.getOrganizations();
	}

	getOrganizations() : void {
		this.organizationServise.getOrganiztions().subscribe(
			organization => {this.organizations = organization });
	}
}