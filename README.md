## **Project to create and store organizations in database** ##

### Backend technologies: ###
* Java
* Spring Boot with Gradle
* Spring Data JPA
* Tomcat
* MySQL

### Frontend technologies: ###
* Angular 2 with Typescript