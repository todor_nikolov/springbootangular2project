CREATE TABLE IF NOT EXISTS organizations
(
    id int(11) NOT NULL AUTO_INCREMENT,
    name varchar(150) NOT NULL,
    reg_number varchar(30) NOT NULL,
    reg_date date NOT NULL,
    owner varchar(150) NOT NULL,
    address varchar(200) NOT NULL,
    postal_code varchar(10) NOT NULL,
    activity_type varchar(200) NOT NULL,
    description text NOT NULL,
    PRIMARY KEY (id)
);