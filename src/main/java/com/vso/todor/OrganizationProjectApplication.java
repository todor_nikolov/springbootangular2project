package com.vso.todor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrganizationProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrganizationProjectApplication.class, args);
	}
	
}
