package com.vso.todor.controller;

import java.io.IOException;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vso.todor.model.Organization;
import com.vso.todor.repository.OrganizationRepository;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class OrganizationController {

	@Autowired
	private OrganizationRepository repository;
	
	@RequestMapping(value = "/new", method = RequestMethod.POST)
	public ResponseEntity<?> createOrganization(@RequestBody String input) {
		
		ObjectMapper mapper = new ObjectMapper();
		Organization organization = null;

		try {
			organization = mapper.readValue(input, Organization.class);
		} catch (JsonParseException e) {
			System.out.println("Json parsing error");
			e.printStackTrace();
		} catch (JsonMappingException e) {
			System.out.println("Json mapping error");
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		repository.save(new Organization(organization.getName(), organization.getRegNumber(), organization.getRegDate(),
				organization.getOwner(), organization.getAddress(), organization.getPostalCode(),
				organization.getActivityType(), organization.getDescription()));

		HttpHeaders httpHeaders = new HttpHeaders();
		return new ResponseEntity<>(null, httpHeaders, HttpStatus.OK);
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public @ResponseBody ArrayList<Organization> allOrganizations() {
		return (ArrayList<Organization>) this.repository.findAll();
	}

}
